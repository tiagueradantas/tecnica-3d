﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlameOffice_Tiago_1708911
{
    interface ILOperacoes
    {
        string GerarHash();
        List<string> GerarLista();
    }
}
